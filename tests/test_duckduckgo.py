import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://duckduckgo.com/')


    # Znalezienie paska wyszukiwania
    search_bar = browser.find_element(By.CSS_SELECTOR, "#search_form_input_homepage")



    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, "#search_button_homepage")

    # Asercje że elementy są widoczne dla użytkownika
    assert search_bar.is_displayed() is True
    assert search_button.is_displayed() is True
    # Szukanie Vistula University
    search_bar.send_keys("Vistula University")
    search_button.click()
    # Sprawdzenie że lista wyników jest dłuższa niż 2
    search_results = browser.find_elements(By.CSS_SELECTOR, '.nrn-react-div')
    assert len(search_results) > 2
    # Sprawdzenie że jeden z tytułów to strona uczelni
    search_results1 = browser.find_elements(By.CSS_SELECTOR, "[data-testid='result-title-a']")

    # Zamknięcie przeglądarki
    time.sleep(3)
    browser.quit()

def test_searching_in_duckduckgo_in_bing():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony duckduckgo
    browser.get('https://bing.com/')


    # Znalezienie paska wyszukiwania
    search_bar = browser.find_element(By.CSS_SELECTOR, "#ssb_form_q")



    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, ".search_svg")

    # Asercje że elementy są widoczne dla użytkownika
    assert search_bar.is_displayed() is True
    assert search_button.is_displayed() is True
